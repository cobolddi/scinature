jQuery(function($){
  'use-strict'

	$(document).ready(function() {
		
		var touch   = $('#resp-menu');
		  var menu  = $('.menu');
		 
		  $(touch).on('click', function(e) {
		    
		    menu.slideToggle();
		  });
		  
		  $(window).resize(function(){
		    var w = $(window).width();

		    // breakpoint
		    if(w > 767 && menu.is(':hidden')) {
		      menu.removeAttr('style');
		    }
		});

		//-----HeaderPadding
	    var headerHeight = $('#header').outerHeight(true);
	    // console.log("Header Height", headerHeight);
	    $('main').css('paddingTop', headerHeight);

	    $(window).scroll(function() {

			if ($(this).scrollTop() > 150){

			  $('header').addClass("shadowBox");

			} else {

			  $('header').removeClass("shadowBox");

			}
		});


		
	});


});


