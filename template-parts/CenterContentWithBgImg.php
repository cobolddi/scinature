<section class="Section CenterContent" style="background: url(assets/img/who-we-are.png) no-repeat;">
	<div class="container SmallContainer">
		<h2 class="BottomBar">Who We Are</h2>
		<h6>POWERED BY NATURE, DRIVEN BY HOPE</h6>
		<p>At SCINATURE, we understand the human body as a series of interconnected systems that, in a simpler past, lived and interacted in harmony with the nature that surrounded us. In this day and age, however, modern life has created a rift, and we can see and feel the disconnection manifesting in our physical and mental spaces. The result: Dis-ease!</p>
		<p>With our 100% plant-based, high-quality, and non-invasive products, SCINATURE aims to reduce this gap, connecting us back to the healing powers of nature. Our products manage and prevent disease, and are backed by state-of-the-art scientific research.</p>
		<a href="#" class="BorderBtn">Know More</a>
	</div>
</section>