<!-- <section class="LeftImgRightContent">
	<div class="LeftImage">
		<img src="assets/img/scinature-science.png" alt="">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 ML-Auto">
				<div class="RightContent">
					<h2>SCINATURE SCIENCE</h2>
					<h6>POWERED BY NATURE, DRIVEN BY HOPE</h6>
					<p>At SCINATURE, we understand the human body as a series of interconnected systems that, in a simpler past, lived and interacted in harmony with the nature that surrounded us. In this day and age, however, modern life has created a rift, and we can see and feel the disconnection manifesting in our physical and mental spaces. The result: Dis-ease! With our 100% plant-based, high-quality, and non-invasive products, SCINATURE aims to reduce this gap, connecting us back to the healing powers of nature. Our products manage and prevent disease, and are backed by state-of-the-art scientific research.</p>
					<a href="#" class="BorderBtn">Know More</a>
				</div>
			</div>
		</div>
	</div>
</section> -->

<section class="LeftImgRightContent">
	<div class="RightContentLeftAlignedImage">
		<div class="SideAlignedImageDiv LeftAlignedImage">
			<img src="assets/img/scinature-science.png" data-aos="fade-right">
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-6 ML-Auto">
					<div class="ContentBlock">
						<h2 class="BottomBar LeftBar">SCINATURE <br>SCIENCE</h2>
						<h6>POWERED BY NATURE, DRIVEN BY HOPE</h6>
						<p>At SCINATURE, we understand the human body as a series of interconnected systems that, in a simpler past, lived and interacted in harmony with the nature that surrounded us. In this day and age, however, modern life has created a rift, and we can see and feel the disconnection manifesting in our physical and mental spaces. The result: Dis-ease! With our 100% plant-based, high-quality, and non-invasive products, SCINATURE aims to reduce this gap, connecting us back to the healing powers of nature. Our products manage and prevent disease, and are backed by state-of-the-art scientific research.</p>
						<a href="#" class="BorderBtn">Know More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="LeftImgRightContent">
	<div class="LeftContentRightAllignedImage" style="background-color: #f5fafd;">
		<div class="SideAlignedImageDiv rightAlignedImage">
			<img src="assets/img/scinature-science.png" data-aos="fade-left">
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="ContentBlock">
						<h2 class="BottomBar LeftBar">RESOURCES FOR ONCOLOGISTS</h2>
						<h6>POWERED BY NATURE, DRIVEN BY HOPE</h6>
						<p>At SCINATURE, we understand the human body as a series of interconnected systems that, in a simpler past, lived and interacted in harmony with the nature that surrounded us. In this day and age, however, modern life has created a rift, and we can see and feel the disconnection manifesting in our physical and mental spaces. The result: Dis-ease! With our 100% plant-based, high-quality, and non-invasive products, SCINATURE aims to reduce this gap, connecting us back to the healing powers of nature. Our products manage and prevent disease, and are backed by state-of-the-art scientific research.</p>
						<a href="#" class="BorderBtn">Know More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>