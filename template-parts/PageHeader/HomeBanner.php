<section class="BannerBlock" style="background: url(assets/img/banner.png) no-repeat;">
	<div class="container">
		<div class="ContentBox">
			<div class="BannerContent">
				<h3>BRIDGING THE GAP BETWEEN</h3>
				<h1>SCIENCE & NATURE</h1>			
				<p>At Scinature, we work to eradicate dis-ease caused by the modern world's obsession of distancing itself with nature. We believe the true solution lies being one with nature and all the abundance it offers.</p>
			</div>
			<img src="assets/img/BannerElement.svg" alt="">
		</div>		
	</div>
</section>