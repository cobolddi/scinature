<section class="Section OurProducts">
	<div class="container">
		<h2 class="BottomBar">OUR PRODUCTS</h2>
		<h6>CANCER MANAGEMENT</h6>
		<div class="row">
			<div class="col-6 col-md-4">
				<a href="#">
					<div class="ProductImage">
						<img src="assets/img/product.png" alt="">
					</div>
					<div class="ProductDetails">
						<h4>CYPRO</h4>
						<ul>
							<li><span>Relief during cancer treatment & chemo</span></li>
							<li><span>Liver and kidney ailments</span></li>
							<li><span>Arthritic disorders</span></li>
						</ul>
					</div>
					<span class="ProductLink"><img src="assets/img/right-arrow.svg" alt=""></span>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="#">
					<div class="ProductImage">
						<img src="assets/img/chempro.png" alt="">
					</div>
					<div class="ProductDetails">
						<h4>CHEMPRO</h4>
						<ul>
							<li><span>Nausea & stomach disorders</span></li>
							<li><span>Mood disorders</span></li>
							<li><span>Inflammatory disorders</span></li>
							<li><span>Acid reﬂux</span></li>
						</ul>
					</div>
					<span class="ProductLink"><img src="assets/img/right-arrow.svg" alt=""></span>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="#">
					<div class="ProductImage">
						<img src="assets/img/zindrol.png" alt="">
					</div>
					<div class="ProductDetails">
						<h4>ZINDROL</h4>
						<ul>
							<li><span>Oxidative damage</span></li>
							<li><span>Liver ailments</span></li>
							<li><span>Asthma</span></li>
							<li><span>Allergy</span></li>
						</ul>
					</div>
					<span class="ProductLink"><img src="assets/img/right-arrow.svg" alt=""></span>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="#">
					<div class="ProductImage">
						<img src="assets/img/productX.png" alt="">
					</div>
					<div class="ProductDetails">
						<h4>PRODUCT X</h4>
						<ul>
							<li><span>Relief during cancer treatment & chemo</span></li>
							<li><span>Liver and kidney ailments</span></li>
							<li><span>Arthritic disorders</span></li>
						</ul>
					</div>
					<span class="ProductLink"><img src="assets/img/right-arrow.svg" alt=""></span>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="#">
					<div class="ProductImage">
						<img src="assets/img/productY.png" alt="">
					</div>
					<div class="ProductDetails">
						<h4>PRODUCT Y</h4>
						<ul>
							<li><span>Nausea & stomach disorders</span></li>
							<li><span>Mood disorders</span></li>
							<li><span>Inflammatory disorders</span></li>
							<li><span>Acid reﬂux</span></li>
						</ul>
					</div>
					<span class="ProductLink"><img src="assets/img/right-arrow.svg" alt=""></span>
				</a>
			</div>
			<div class="col-6 col-md-4">
				<a href="#">
					<div class="ProductImage">
						<img src="assets/img/productZ.png" alt="">
					</div>
					<div class="ProductDetails">
						<h4>PRODUCT Z</h4>
						<ul>
							<li><span>Oxidative damage</span></li>
							<li><span>Liver ailments</span></li>
							<li><span>Asthma</span></li>
							<li><span>Allergy</span></li>
						</ul>
					</div>
					<span class="ProductLink"><img src="assets/img/right-arrow.svg" alt=""></span>
				</a>
			</div>
		</div>
		<div class="ProductCTA">
			<a href="#" class="BorderBtn">Know More</a><a href="#" class="BorderBtn">BUY ONLINE</a>
		</div>
	</div>
</section>