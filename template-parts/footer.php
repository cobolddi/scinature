
</main>
<footer>
	<div class="FooterMenuBlock">
		<div class="container">
			<div class="row">
				<div class="col-6 col-md-6 MobileOnly">
					<div class="BrandDetails">
						<a href="#"><img src="assets/img/whitelogo.png"></a>
						<h6>SCINATURE LABS PVT LTD</h6>
						<p>Farm 4, 2nd Floor, Club Drive<br> Ghitorni, New Delhi - 110030</p>
					</div>
				</div>
				<div class="col-6 col-md-2">
					<h6>Scinature</h6>
					<ul>
						<li><a href="">About Us</a></li>
						<li><a href="">Legacy</a></li>
						<li><a href="">Vision</a></li>
						<li><a href="">Brand Story</a></li>
						<li><a href="">Scinature Science</a></li>
						<li><a href="">DRF Difference</a></li>
						<li><a href="">Scientific Reports</a></li>
					</ul>
				</div>
				<div class="col-6 col-md-2">
					<h6>PRODUCTS</h6>
					<ul>
						<li><a href="">Cypro</a></li>
						<li><a href="">Chempro</a></li>
						<li><a href="">Zindrol</a></li>
						<li><a href="">Product X</a></li>
						<li><a href="">Product Y</a></li>
						<li><a href="">Product Z</a></li>
					</ul>
				</div>
				<div class="col-6 col-md-2">
					<h6>HELPFUL LINKS</h6>
					<ul>
						<li><a href="">Testing Reports</a></li>
						<li><a href="">Video Instructions</a></li>
						<li><a href="">FAQs</a></li>
						<li><a href="">Feedback</a></li>
						<li><a href="">Contact Us</a></li>
						<li><a href="">My Account</a></li>
						<li><a href="">Return Policy</a></li>
					</ul>
				</div>
				<div class="col-6 col-md-6 DesktopOnly">
					<div class="BrandDetails">
						<a href="#"><img src="assets/img/whitelogo.png"></a>
						<h6>SCINATURE LABS PVT LTD</h6>
						<p>Farm 4, 2nd Floor, Club Drive<br> Ghitorni, New Delhi - 110030</p>
					</div>
				</div>
			</div>
			<ul class="MultipleOption">
				<li><a href="tel:1800 102 6121"><img src="assets/img/headset.png">1800 102 6121</a></li>
				<li><a href=""><img src="assets/img/credit-card.png">Secure Transactions</a></li>
				<li><a href=""><img src="assets/img/delivery-truck.png">Quick Doorstep Delivery</a></li>
				<li><a href=""><img src="assets/img/money.png">All Major Cards Accepted</a></li>
				<li><a href=""><img src="assets/img/shopping-cart.png">Returns & Refunds</a></li>
			</ul>
		</div>
	</div>
	<div class="BottomFooterBlock">
		<div class="container">
			<ul>
				<li>Copyright © <script>document.write(new Date().getFullYear())</script> Scinature Labs Pvt Ltd</li>
				<li>Delivered by <a href="">Cobold Digital</a></li>
			</ul>
		</div>
	</div>
</footer>

<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>