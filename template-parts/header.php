<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Scinature</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="At Scinature, we work to eradicate dis-ease caused by the modern world's obsession of distancing itself with nature.">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/favicon.png">	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>

<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-4 col-md-2">
				<a href="" class="logo"><img src="assets/img/logo.png" alt=""></a>
			</div>
			<div class="col-10 col-md-10 DesktopOnly">
				<div class="TopMenuBlock">
					<ul class="TopMenu">
						<li><a href="">Feedback</a></li>
						<li><a href="">FAQs</a></li>
						<li><a href="">Contact Us</a></li>
						<li><a href=""><img src="assets/img/cart.svg" alt=""></a></li>
						<li><a href=""><img src="assets/img/user.svg" alt=""></a></li>
						<li>
							<ul class="SocialIcons">                            
	                            <li>
	                                <a href="#" target="_blank">
	                                    <svg>
	                                        <use xlink:href="assets/img/cobold-sprite.svg#facebook-icon"></use>
	                                    </svg>
	                                </a>
	                            </li>
	                            
	                            <li>
	                                <a href="#" target="_blank">
	                                    <svg>
	                                        <use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use>
	                                    </svg>
	                                </a>
	                            </li>
	                            
	                            <li>
	                                <a href="#" target="_blank">
	                                    <svg>
	                                        <use xlink:href="assets/img/cobold-sprite.svg#linkedin-icon"></use>
	                                    </svg>
	                                </a>
	                            </li>
	                        </ul>
						</li>
					</ul>
				</div>
				<div class="NavigationBlock">
					<div>
						<ul>
							<li><a href="#">ABOUT US</a></li>
							<li><a href="#">SCINATURE SCIENCE</a></li>
							<li><a href="#">OUR PRODUCTS</a></li>
							<li><a href="#">ONLINE SHOP</a></li>
							<li><a href="#">FOR ONCOLOGISTS</a></li>
						</ul>
					</div>
				</div>
				
			</div>
			<div class="col-8 col-md-10 MobileOnly">
				<div class="TopMenuBlock">
					<ul class="TopMenu">
						<li><a href=""><img src="assets/img/cart.svg" alt=""></a></li>
						<li><a href=""><img src="assets/img/user.svg" alt=""></a></li>
					</ul>
				</div>
				<nav>
				  <a id="resp-menu" class="responsive-menu" href="#">
				  	<span></span>
				  	<span></span>
				  	<span></span>
				  </a>
				</nav>
			</div>
		</div>
	</div>

	<div>
		<ul class="menu">
			<li><a href="#">ABOUT US</a>
				<ul>
					<li><a href="#">ABOUT US</a></li>
					<li><a href="#">SCINATURE SCIENCE</a></li>
					<li><a href="#">OUR PRODUCTS</a></li>
					<li><a href="#">ONLINE SHOP</a></li>
					<li><a href="#">FOR ONCOLOGISTS</a></li>
				</ul>
			</li>
			<li><a href="#">SCINATURE SCIENCE</a></li>
			<li><a href="#">OUR PRODUCTS</a></li>
			<li><a href="#">ONLINE SHOP</a></li>
			<li><a href="#">FOR ONCOLOGISTS</a></li>
			<li><a href="">Feedback</a></li>
			<li><a href="">FAQs</a></li>
			<li><a href="">Contact Us</a></li>
		</ul>
	</div>
</header>
<main>